==============================
Cross compilation on Ubunut
==============================
With Qt build environment and qtcharts available, run "qmake", and then "make".


==============================
Run RnnPdmAnomalyDetection on target
==============================
Stop Matrix GUI if needed: /etc/init.d/matrix-gui stop

Terminal 1: ./RnnPdmAnomalyDetection -0.0001841035315 0.004191935886 0.01918238488 0.01933241767 1.5
This will open a Qt GUI for the demo.

Terminal 2: direct sensor data input to /tmp/myfifo, e.g.,
            cat ./logs/normal45-270-v100-with-friction2-iter10-15.log > /tmp/myfifo
            cat ./logs/normal100-anomaly150-normal100.log > /tmp/myfifo
            cat ./logs/normal270-anomaly170-normal270.log > /tmp/myfifo

View the sensor data and anomaly detection results from the Qt GUI.
