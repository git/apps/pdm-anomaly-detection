/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Qt Charts module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/
/*
 * Copyright (c) 2019, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef DATASOURCE_H
#define DATASOURCE_H

#include <QtCore/QObject>
#include <QtCharts/QAbstractSeries>

#define DISPLAY_VECTOR 6

class QQuickView;

QT_CHARTS_USE_NAMESPACE

class DataSource : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString pdmStage READ getPdmStage WRITE setPdmStage NOTIFY pdmStageChanged)
    Q_PROPERTY(QString anomalyDetection READ getAnomalyDetection WRITE setAnomalyDetection NOTIFY anomalyDetectionChanged)

public:
    explicit DataSource(QQuickView *appViewer, QObject *parent = 0);

    QString getPdmStage()
    {
        return pdmStage;
    }

    void setPdmStage(const QString &text)
    {
        if (text == pdmStage) return;
        pdmStage = text;
        emit pdmStageChanged();
    }

    QString getAnomalyDetection()
    {
        return anomalyDetection;
    }

    void setAnomalyDetection(const QString &text)
    {
        if (text == anomalyDetection) return;
        anomalyDetection = text;
        emit anomalyDetectionChanged();
    }

Q_SIGNALS:
    void pdmStageChanged();
    void anomalyDetectionChanged();

public slots:
    void generateData(int sel_waveform, int type, int colCount);
    void update(int sel_waveform, QAbstractSeries *series);
    void getdata(void);

private:
    QQuickView *m_appViewer;
    QVector<QPointF> m_data[DISPLAY_VECTOR];

    int m_sampleCount, m_last_read;
    int m_overlapWindow;

    bool printCalibration, printDetection;

    QString pdmStage, anomalyDetection;

    double m_samplesArray[4096*DISPLAY_VECTOR]; // Keep in sync with max sample count selection in qml file
};

#endif // DATASOURCE_H
