/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Qt Charts module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/
/*
 * Copyright (c) 2019, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <QtWidgets/QApplication>
#include <QtQml/QQmlContext>
#include <QtQuick/QQuickView>
#include <QtQml/QQmlEngine>
#include <QtCore/QDir>

#include "datasource.h"
#include "motor_pdm.h"

#include <iostream>
#include <thread>

/*********************************************************************************
 * This is the main function for PdM (predictive maintenance) - anomaly detection
 * It uses Qt/QML for real-time display of the sensor data and detection results.
 * motor_pdm_process is the main thread for anomaly detection, using RNN;
 * It also starts another thread of uart_stream_parser to get the sensor input.
 *********************************************************************************/
pdmContext_t pdmContext;
int main(int argc, char *argv[])
{
    int ret;
    /* Get normalization parameters and relative threshold */
    if(argc < 5) {
        printf("\nUsage:%s mu1 mu2 sig1 sig2 relative-threshold\n", argv[0]);
        printf("\nTo set the relative threshold value, use an additional positive value >= 1\n");
        return -1;
    }
    pdmContext.mu1  = atof(argv[1]);
    pdmContext.mu2  = atof(argv[2]);
    pdmContext.sig1 = atof(argv[3]);
    pdmContext.sig2 = atof(argv[4]);

    if(argc > 5) {
        pdmContext.relative_threshold = atof(argv[5]);
    } else {
        pdmContext.relative_threshold = 1.5;
    }

    /* Qt Charts uses Qt Graphics View Framework for drawing, therefore QApplication must be used. */
    QApplication app(argc, argv);

    QQuickView viewer;

    /* The following are needed to make examples run without having to install the module */
    /* in desktop environments.                                                           */
#ifdef Q_OS_WIN
    QString extraImportPath(QStringLiteral("%1/../../../../%2"));
#else
    QString extraImportPath(QStringLiteral("%1/../../../%2"));
#endif
    viewer.engine()->addImportPath(extraImportPath.arg(QGuiApplication::applicationDirPath(),
                                      QString::fromLatin1("qml")));
    QObject::connect(viewer.engine(), &QQmlEngine::quit, &viewer, &QWindow::close);

    /* Set the title of the Qt window */
    viewer.setTitle(QStringLiteral("Deep Learning based Predictive Maintenance Demo on Sitara Edge Devices"));

    /* Start the thread for conducting anomaly detection on motor sensor data */
    std::thread motor_pdm_process_thread (motor_pdm_process);

    /* Set up the data source object for displaying sensor data and detection results */
    DataSource dataSource(&viewer);
    viewer.rootContext()->setContextProperty("dataSource", &dataSource);

    /* Start the Qt display */
    viewer.setSource(QUrl("qrc:/qml/pdm-anomaly-detection/main.qml"));
    viewer.setResizeMode(QQuickView::SizeRootObjectToView);
    viewer.setColor(QColor("#f5f5dc"));
    viewer.show();

    ret = app.exec();

    /* Detach the thread: avoid the message of "terminate called without an active exception"
     * when closing the Qt window  */
    motor_pdm_process_thread.detach();

    return ret;
}
