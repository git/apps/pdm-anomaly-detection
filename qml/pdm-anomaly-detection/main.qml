/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Qt Charts module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/
/*
 * Copyright (c) 2019, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

import QtQuick 2.0

Item {
    id: main
    width: 1920
    height: 1080

    /* Demo title at the top */
    Text {
        anchors.top: parent.top
        anchors.topMargin: 50
        anchors.left: parent.left
        anchors.leftMargin: 50
        text: "Deep Learning based Predictive Maintenance (PdM) on Sitara Edge Devices"
        font.family: "Helvetica"
        font.pointSize: 36 
        font.bold: true
        color: "red"
    }

    /* TI logo image at the bottom right */
    Image {
        source: "./ti-logo.png"
        scale: 0.35
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 35
        anchors.right: parent.right
        anchors.rightMargin: -80
    }

    /* Control button at bottom left to hide/show the images for setup, call flow, and system model */
    ControlPanel {
        id: controlPanel
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 75
        anchors.left: parent.left
        anchors.leftMargin: 60
        onImageDisplayChanged: {
            picmotordrive.visible = (!picmotordrive.visible);
            picanomalydetection.visible = (!picanomalydetection.visible);
            picsystemmodel.visible = (!picsystemmodel.visible);
        }
    }

    /* Display sensor readings at the left panel */
    ScopeView {
        id: scopeView
        anchors.top: parent.top
        anchors.topMargin: 200 
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 100
        anchors.left: parent.left
        anchors.leftMargin: 50
        anchors.right: parent.right
        anchors.rightMargin: 950
    }

    /* Image for motor drive setup */
    Image {
        id: picmotordrive
        source: "./motor-drive.png"
        scale: 0.83
        visible: true
        anchors.top: scopeView.top
        anchors.topMargin: 80
        anchors.right: scopeView.right
        anchors.rightMargin: 200
    }

    /* Display prediction error and anomaly from RNN prediction */
    PdmView {
        id: pdmView
        anchors.top: parent.top
        anchors.topMargin: 200
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 100
        anchors.left: scopeView.right
        anchors.right: parent.right
        anchors.rightMargin: 50
    }

    /* Image for call flow of anomaly detection */
    Image {
        id: picanomalydetection
        source: "./anomaly-detection.png"
        scale: 0.85
        visible: true
        anchors.top: pdmView.top
        anchors.topMargin: 70
        anchors.right: pdmView.right
        anchors.rightMargin: 370
    }

    /* Image for system model of anomaly detection */
    Image {
        id: picsystemmodel
        source: "./system-model.png"
        scale: 0.8
        visible: true
        anchors.top: pdmView.top
        anchors.topMargin:160
        anchors.right: pdmView.right
        anchors.rightMargin: 15
    }

    /* Caption for sensor reading panel */
    Text {
        text: "Real-time Sensor Input"
        font.family: "Helvetica"
        font.pointSize: 22
        color: "red"
        anchors.top: parent.top
        anchors.topMargin: 150
        anchors.left: parent.left
        anchors.leftMargin: 280
    }

    /* Caption for anomaly detection panel */
    Text {
        text: "Real-time Anomaly Detection"
        font.family: "Helvetica"
        font.pointSize: 22
        color: "red"
        anchors.top: parent.top
        anchors.topMargin: 150
        anchors.right: parent.right
        anchors.rightMargin: 280
    }

    /* Status string: calibration or looking for anomaly */
    Text {
        text: dataSource.pdmStage
        font.family: "Helvetica"
        font.pointSize: 22
        color: "green"
        anchors.top: parent.top
        anchors.topMargin: 650
        anchors.right: parent.right
        anchors.rightMargin: 460
    }

    /* Detection result string: empty for anomaly detected */
    Text {
        text: dataSource.anomalyDetection
        font.family: "Helvetica"
        font.pointSize: 24
        color: "red"
        anchors.top: parent.top
        anchors.topMargin: 650
        anchors.right: parent.right
        anchors.rightMargin: 125
    }
}
