===================================================
LSTM_model.h created from the ONNX model
===================================================

The ONNX model is converted with python scripts in this folder
to create the CPP header file (LSTM_model.h) with initialized 
data structures, which is then compiled to establish the 
Recurrent Neural Network (RNN) inference libarary for anomaly 
detection.

  python ./make_header.py -m ./timotor20190227raw.onnx

  -m/--model: onnx model filename
