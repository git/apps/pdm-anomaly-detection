/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Qt Charts module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/
/*
 * Copyright (c) 2019, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <iostream>
#include "datasource.h"
#include <QtCharts/QXYSeries>
#include <QtCharts/QAreaSeries>
#include <QtQuick/QQuickView>
#include <QtQuick/QQuickItem>
#include <QtCore/QDebug>

#include <QtCore/QtMath>

QT_CHARTS_USE_NAMESPACE

Q_DECLARE_METATYPE(QAbstractSeries *)
Q_DECLARE_METATYPE(QAbstractAxis *)

DataSource::DataSource(QQuickView *appViewer, QObject *parent) :
    QObject(parent),
    m_appViewer(appViewer)
{
    qRegisterMetaType<QAbstractSeries*>();
    qRegisterMetaType<QAbstractAxis*>();

    for (int i=0; i<DISPLAY_VECTOR; i++)
    {
        generateData(i, 0, 256);
    }

    setPdmStage("Waiting for sensor data......");
    setAnomalyDetection("");

    printCalibration = true;
    printDetection = true;
}

void DataSource::update(int sel_waveform, QAbstractSeries *series)
{
    if (series && (m_last_read > 0)) {
        QXYSeries *xySeries = static_cast<QXYSeries *>(series);
        QVector<QPointF> points = m_data[sel_waveform];
        // Use replace instead of clear + append, it's optimized for performance
        qreal x(0);
        qreal y(0);
        bool printAnomaly = true;
        for (int j = 0; j < m_sampleCount; j ++) {
            y = m_samplesArray[DISPLAY_VECTOR*j + sel_waveform];
            x = j;
            /* Time series 4 is for displaying the anomaly detection threshold
             * (positive value). It is also carrying the stage information
             * (calibration or anomaly detection): offset of 100 for calibration;
             * offset of -100 for anomaly detection, as used in motor_pdm_process() */
            if (sel_waveform==4) {
                if (y > 100) {
                    y = y - 100;
                    setPdmStage("Calibration......");
                    /* Print the calibration message once */
                    if (printCalibration) {
                        std::cout << "Calibration......" << std::endl;
                        printCalibration = false;
                    }
                }
                if (y < -50) {
                    y = y + 100;
                    setPdmStage("Looking for anomaly......");
                    /* Print the detection message once */
                    if (printDetection) {
                        std::cout << "Looking for anomaly......" << std::endl;
                        printDetection = false;
                    }
                }
            }
            points.replace(j, QPointF(x, y));
            /* Time series 5 is for displaying the anomaly detection results:
             * value 0 indicates the normal operation, and positive value
             * anomaly detection threshold*ratio(>1) indicates anomalies */
            if ((sel_waveform==5) && (y>0)) {
                setAnomalyDetection("Anomaly detected!!!");
                /* Print the anomaly message once per the series update.
                 * This allows anomaly messages printed out for the duration
                 * of anomalies without slowing down the QT GUI display. */
                if (printAnomaly) {
                    std::cout << "Anomaly detected!!!" << std::endl;
                    printAnomaly = false;
                    /* Enable print of the detection message after the anomaly message
                     * in printed out. This is to allow going back to the detection
                     * message when there are no more anomalies. */
                    printDetection = true;
                }
            }
            if ((sel_waveform==5) && (y<=0)) {
                setAnomalyDetection("");
            }
        }
        xySeries->replace(points);
    }
}

int motor_pdm_get_data(int samples_count, double *samples_array);
void DataSource::getdata(void)
{
        m_last_read = motor_pdm_get_data(m_sampleCount, m_samplesArray);
}

void DataSource::generateData(int sel_waveform, int type, int colCount)
{
   // Remove previous data
    m_data[sel_waveform].clear();

    std::cout << std::endl << "__generating (" << sel_waveform << ":" << colCount << " type:" << type << ")____" << std::endl;

    m_sampleCount = colCount;
    // Append the new data depending on the type
    QVector<QPointF> points;
    points.reserve(colCount);
    m_overlapWindow = type;
    for (int j(0); j < colCount; j++) {
       qreal x(0);
       qreal y(0);
       points.append(QPointF(x, y));
    }
    m_data[sel_waveform] = points;
}
