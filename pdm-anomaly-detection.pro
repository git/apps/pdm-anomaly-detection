QT += charts qml quick

DEFINES += QRANDOMGENERATOR_MISSING

HEADERS += \
    datasource.h\
    config.h \
    LSTM_model.h \
    onnx_model.h \
    lstm_infer.h \
    motor_pdm.h \
    psensors.h \
    motor_pdm_loc.h

SOURCES += \
    main.cpp \
    datasource.cpp \
    psensors.cpp \
    motor_pdm.cpp \
    lstm_infer.cpp \
    onnx_model.cpp \
    config.cpp \

RESOURCES += \
    resources.qrc

DISTFILES += \
    qml/pdm-anomaly-detection/*


# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

TARGET = RnnPdmAnomalyDetection
