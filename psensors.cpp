/*
 * Copyright (c) 2019, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <fcntl.h> 
#include <sys/stat.h> 
#include <sys/types.h> 
#include <unistd.h>

#include "psensors.h"

#include <iostream>
#include <thread>
#include <chrono>

//#define VERBOSE

/* Define the buffer to store the incoming sensor data and the read/write counter*/
static int psensors_rd_data_cnt = 0;
static int psensors_wr_data_cnt = 0;
static float psensors_data_ready_to_use[MAX_BUFFER];

/* Send the buffered sensor data to downstream for further processing */
int uart_get_data(int samples_count, float *samples_array)
{
    int rd_idx;
#ifdef VERBOSE
    printf ("\nwr=%d rd=%d cnt=%d ", psensors_wr_data_cnt, psensors_rd_data_cnt, samples_count);
#endif
    /* When the number of sensor data points reach samples_count, send them to downstream */
    if(psensors_wr_data_cnt >= (psensors_rd_data_cnt + samples_count*3)) {
        for (int i = 0; i < samples_count; i ++) {
            rd_idx = psensors_rd_data_cnt % MAX_BUFFER;
            samples_array[3*i + 0] = psensors_data_ready_to_use[rd_idx + 0];
            samples_array[3*i + 1] = psensors_data_ready_to_use[rd_idx + 1];
            samples_array[3*i + 2] = psensors_data_ready_to_use[rd_idx + 2];
            psensors_rd_data_cnt += 3;
        }
        return samples_count;
    } else return 0;
}

/* Parse the incoming sensor data and write the parsed sensor data into the buffer */
void uart_stream_parser(int mode_op)
{
    const char *log_filename = mode_op & 0x01 ? "parser.txt" : NULL;
    FILE *fout;
    int wr_idx = 0;
#ifdef VERBOSE
    int prev_sync = 0;
#endif
    int prev_cnt  = 0;
    unsigned int val1, val_phases, val_position;
    short int_phase1, int_phase2;
    float phase1, phase2, position;
    unsigned char all_zeros[256];
    unsigned char line_in[256];
    int sync_i = (1 << 31);
    int cnt, err_flag = 0;
    int fd;
    /* FIFO file path */
    const char * pdmfifo = "/tmp/pdmfifo";

    memset(all_zeros, 0, 256);
    memset(line_in, 0, 256);

    /* Creating the named file(FIFO) */
    mkfifo(pdmfifo, 0666);

    /* Open FIFO for Read only */
    fd = open(pdmfifo, O_RDONLY);

    if(log_filename) {
        fout = fopen(log_filename, "wt");
    }

    for (int i = 0; ; i ++) {
        /* Shift delay line left for one byte */
        for(int j = 1; j < 256; j ++) {
            line_in[j - 1] = line_in[j];
        }
        if(read(fd, &line_in[255], 1) != 1) {
            line_in[255] = 0;
            /* Continue processing until all buffered samples are processed */
            if(psensors_wr_data_cnt <= psensors_rd_data_cnt) {
                /* No unprocessed bytes left, sleep for some time as there is nothing on input */
                std::this_thread::sleep_for(std::chrono::milliseconds(20));
                continue;
            }
        }

        /* Writting too fast, do write side throttling */
        if((mode_op & 0x02) && (psensors_wr_data_cnt > (psensors_rd_data_cnt + MAX_BUFFER - 16))) {
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }

        /* Parsing the magic word and counter at the beginning */
        if(line_in[0] == 0xce &&
           line_in[1] == 0xfa &&
           line_in[2] == 0xbe &&
           line_in[3] == 0xba) {
            cnt = *(unsigned int *)&line_in[1*4];
            sync_i = i + 2*4;
#ifdef VERBOSE
            printf ("\n%08x, %8d:%8d ... %8d[%8d]\n", *(unsigned int *)&line_in[0], i, cnt,
                                                      sync_i - prev_sync, cnt - prev_cnt);
            prev_sync = sync_i;
#endif
            if((i >= 4*1024) && (cnt - prev_cnt) != 1023) {
                err_flag |= 1;
            }
            prev_cnt  = cnt;
        }
        if((i - sync_i) >= 0) {
#ifdef VERBOSE
            if(((i - sync_i) % (2*4)) == 0) {
                printf ("\n[%8d, %8d] ... ", i, sync_i);
            }
#endif
            if(((i - sync_i) % 4)  == 0) {
                /* Get position data */
                val1 = *(unsigned int *)&line_in[0];
#ifdef VERBOSE
                printf ("%08x  ", val1);
#endif
                /* Get phase current data */
                if(((i - sync_i) % 8) == 0) {
                    val_phases = val1;
                } else {
                    /* Calculate the position and the two phase currents */
                    val_position = (int)val1;
                    int_phase1 = (short)(val_phases >> 16);
                    int_phase2 = (short)((val_phases << 16) >> 16);
                    phase1     = (float)int_phase1 / 32767.0;
                    phase2     = (float)int_phase2 / 32767.0;
                    position   = (float)val_position / 33554432.0f;

                    /* Throttling when there is no incoming sensor data */
                    if(phase1 == 0) {
                        std::this_thread::sleep_for(std::chrono::milliseconds(20));
                        continue;
                    }

                    if(log_filename) {
                        fprintf(fout, "%8.5f %8.5f %8.5f\n", phase1, phase2, position);
                    }
#ifdef VERBOSE
                    printf("... %8.5f %8.5f %8.5f", phase1, phase2, position);
#endif
                    /* Write the sensor data to the buffer array */
                    wr_idx = psensors_wr_data_cnt % MAX_BUFFER;
                    psensors_data_ready_to_use[wr_idx + 0] = phase1;
                    psensors_data_ready_to_use[wr_idx + 1] = phase2;
                    psensors_data_ready_to_use[wr_idx + 2] = position;
                    psensors_wr_data_cnt += 3;
                }
            }
        }
    }
    printf("\nEnd of stream!!!\n"); fflush(stdout);
    if(log_filename) {
        fclose(fout);
    }
    close(fd);
}
/** nothing past this point **/
