/*
 * Copyright (c) 2019, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef MOTOR_PDM_LOC_H
#define MOTOR_PDM_LOC_H

/* Filter definitions */
/* Maximum number of inputs that can be handled in one function call */
#define SAMPLES 200
/* Maximum length of filter than can be handled */
#define FIRFLT_LEN 1001
/* Buffer to hold all of the input samples */
#define BUFFER_LEN (FIRFLT_LEN - 1 + SAMPLES)
/* FIR filter coefficients */
double coeffs[ FIRFLT_LEN ];
/* Down-sampling ratio*/
#define DRATIO 200

/* Anomaly detection parameters*/
#define START_FIND_THRESHOLD 150
#define STOP_FIND_THRESHOLD  1500
#define HANGOVER_TIME        20

/* Buffering for Qt display definitions */
#define DISPLAY_VECTOR 6
#define DISPLAY_MAX_BUFFER (DISPLAY_VECTOR*64*4096)
static int display_rd_data_cnt = 0;
static int display_wr_data_cnt = 0;
static double display_data_ready_to_use[DISPLAY_MAX_BUFFER];

void firFloatInit( double *insamp, int len_delay_line);
void firFloat( double *coeffs, double *insamp, int filterLength,
               double *input,  int length,
               double *output);

#endif //MOTOR_PDM_LOC_H
